<?php

namespace Drupal\Tests\jsnippet\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests functionality of jsnippet.
 *
 * @group jsnippet
 */
class JSnippetAdminTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jsnippet_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that we can add (or not add) our snippet.
   */
  public function testJSnippetAdmin() {
    $this->drupalLogin($this->drupalCreateUser([
      'administer snippets',
    ]));
    // Confirm the snippet in config is visible.
    $this->drupalGet('/admin/structure/snippet');
    $this->assertSession()->pageTextContains('Add class');
    $this->assertSession()->pageTextContains('add_class');

    // Edit it.
    $this->drupalGet('/admin/structure/snippet/add_class');
    $this->submitForm(['label' => 'Different label'], 'Update Snippet');
    $this->drupalGet('/admin/structure/snippet');
    $this->assertSession()->pageTextNotContains('Add class');
    $this->assertSession()->pageTextContains('Different label');
    $this->assertSession()->pageTextContains('add_class');

    // Make a new one.
    $this->drupalGet('/admin/structure/snippet/add');
    $this->submitForm([
      'label' => 'Style snippet',
      'id' => 'style_snippet',
      'type' => 'css',
      'snippet' => 'body { color: red }'
    ], 'Create Snippet');
    $this->drupalGet('/admin/structure/snippet');
    $this->assertSession()->pageTextContains('Style snippet');
    $this->assertSession()->pageTextContains('style_snippet');

    // Delete it.
    $this->drupalGet('/admin/structure/snippet/style_snippet/delete');
    $this->assertSession()->pageTextContains('Are you sure you want to delete snippet');
    $this->submitForm([], 'Delete Snippet');
    $this->assertSession()->statusMessageContains('Snippet Style snippet was deleted.');
    $this->assertSession()->pageTextContains('Different label');
    $this->assertSession()->pageTextContains('add_class');
    $this->assertSession()->pageTextNotContains('style_snippet');
  }

}
