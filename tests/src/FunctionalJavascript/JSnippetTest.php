<?php

namespace Drupal\Tests\jsnippet\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;

/**
 * Tests functionality of jsnippet.
 *
 * @group jsnippet
 */
class JSnippetTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jsnippet_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that we can add (or not add) our snippet.
   */
  public function testJSnippet() {
    $page = Node::create([
      'type' => 'page',
      'title' => 'Page with snippet',
      'field_jsnippet' => 'add_class',
    ]);
    $page->save();
    $this->drupalLogin($this->drupalCreateUser([
      'access content',
    ]));
    $this->drupalGet('node/' . $page->id());
    $this->assertSession()->pageTextContains('Page with snippet');
    $this->assertSession()->elementExists('css', 'body.jsnippet-class');

    $page = Node::create([
      'type' => 'page',
      'title' => 'Page sans snippet',
    ]);
    $page->save();
    $this->drupalGet('node/' . $page->id());
    $this->assertSession()->pageTextContains('Page sans snippet');
    $this->assertSession()->elementNotExists('css', 'body.jsnippet-class');
  }

}
